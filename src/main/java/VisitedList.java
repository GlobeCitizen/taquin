import java.util.ArrayList;

public class VisitedList {
    private ArrayList<int[][]> visited = new ArrayList<>();
    public void add(int[][] mat) {
        visited.add(mat);
    }

    public boolean checkMat(int[][] mat1, int [][] mat2) {
        boolean check = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (mat1[i][j] != mat2[i][j]) {
                    check = false;
                }
            }
        }
        return check;
    }

    public boolean belongsTo(int[][] b) {
        boolean belongs = false;

        for(int[][] board : visited) {
            if (checkMat(board, b))
                belongs = true;
        }
        return belongs;
    }

    public int[][] getFirst(){
        int [][] first = visited.get(0);
        visited.remove(0);
        return first;
    }

    public int getSize() {
        return visited.size();
    }

}
