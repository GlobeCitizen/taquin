import java.util.Comparator;

public class Comparateur implements Comparator<GameBoard> {
    @Override
    public int compare(GameBoard o1, GameBoard o2) {
        int c = o2.getHeuristic() - o1.getHeuristic();
        if (c < 0) {
            return -1; // o2 greater than o1
        } else if (c > 0) {
            return 1; // o1 greater than o2
        } else {
            return 0; // o1 equals o2
        }
    }
}

