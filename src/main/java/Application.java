import java.util.*;

public class Application {

    private static List<GameBoard> possible_movements(GameBoard b){
        List<GameBoard> possible_movements = new ArrayList<>();
        if (b.getPointer()[0] == 0) {
            b.move_up();
            possible_movements.add(b.clone());
            b.move_down();
        }

        if (b.getPointer()[0] == 1) {
            b.move_down();
            possible_movements.add(b.clone());
            b.move_up();
            b.move_up();
            possible_movements.add(b.clone());
            b.move_down();
        }

        if (b.getPointer()[0] == 2) {
            b.move_down();
            possible_movements.add(b.clone());
            b.move_up();
        }

        if (b.getPointer()[1] == 0) {
            b.move_left();
            possible_movements.add(b.clone());
            b.move_right();
        }

        if (b.getPointer()[1] == 1) {
            b.move_right();
            possible_movements.add(b.clone());
            b.move_left();
            b.move_left();
            possible_movements.add(b.clone());
            b.move_right();
        }

        if (b.getPointer()[1] == 2) {
            b.move_right();
            possible_movements.add(b.clone());
            b.move_left();
        }
        return possible_movements;
    }

    public static void showStep(int[][] board) {
        for(int i = 0; i<3; i++){
            System.out.println(board[i][0] + " " + board[i][1] + " " + board[i][2]);
        }
        System.out.println("------------------------------");
    }
    public static void reconstitutePath(VisitedList v) {
        int[][] node;
        while (v.getSize() != 0) {
            node = v.getFirst();
            showStep(node);
        }
    }

    public static boolean checkMat(int[][] mat1, int [][] mat2) {
        boolean check = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (mat1[i][j] != mat2[i][j]) {
                    check = false;
                }
            }
        }
        return check;
    }


    public static boolean is_in(PriorityQueue<GameBoard> pq, GameBoard b){
        boolean check = false;
        for (GameBoard board: pq) {
            if(checkMat(board.getBoard(), b.getBoard()))
                check = true;
        }
        return check;
    }

    public static void main(String[] args) {
        int[][] start = {{1, 2, 3}, {4, 0, 5}, {6, 7, 8}};
        int steps = 0;
        GameBoard initial_board = new GameBoard(start);

        Scanner sc = new Scanner(System.in);
        PriorityQueue<GameBoard> pq = new PriorityQueue<>(new Comparateur());
        VisitedList v = new VisitedList();
        List<GameBoard> possible_movements;

        initial_board.show();
        pq.add(initial_board);
        while(pq.peek().getHeuristic() != 9)
        {
            GameBoard gboard = pq.poll();
            possible_movements = possible_movements(gboard);
            for (GameBoard board: possible_movements) {
                if(!v.belongsTo(board.getBoard()) && !is_in(pq, board))
                    pq.add(board);
            }
            if(!v.belongsTo(pq.peek().getBoard())) {
                v.add(pq.peek().getBoard());
            }
            steps++;
        }
        reconstitutePath(v);
        System.out.println("Solution Trouvé en " + steps + " étapes :");
    }
}
