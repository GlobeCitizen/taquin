import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class GameBoard {
    private int[][] board;
    private int[] pointer;
    private int heuristic;

    public GameBoard(int[][] b) {
        this.pointer = new int[2];
        this.heuristic = 0;
        this.board = b;
        find_pointer();
    }

    public void find_pointer() {
        for (int i=0;i<3;i++) {
            for (int j=0; j<3; j++){
                if(board[i][j] == 0){
                    pointer[0] = i;
                    pointer[1] = j;
                }
            }
        }
    }

    public GameBoard clone(){
        int[][] board_clone = new int[3][3];
        for (int i=0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                board_clone[i][j] = board[i][j];
            }
        }
        GameBoard clone = new GameBoard(board_clone);
        return clone;
    }
    // Il manque les verifications des bordures du jeu.
    public void move_up()
    {
        int x = pointer[0];
        int y = pointer[1];

        board[x][y] = board[x + 1][y];
        board[x + 1][y] = 0;
        pointer[0] += 1;
    }

    public void move_down()
    {
        int x = pointer[0];
        int y = pointer[1];

        board[x][y] = board[x - 1][y];
        board[x - 1][y] = 0;
        pointer[0] -= 1;
    }

    public void move_left()
    {
        int x = pointer[0];
        int y = pointer[1];


        board[x][y] = board[x][y + 1];
        board[x][y + 1] = 0;
        pointer[1] += 1;
    }


    public void move_right()
    {
        int x = pointer[0];
        int y = pointer[1];

        board[x][y] = board[x][y - 1];
        board[x][y - 1] = 0;
        pointer[1] -= 1;
    }

    public int[][] getBoard() {
        return board;
    }

    public int[] getPointer() {
        return pointer;
    }

    public int getHeuristic() {
        Heuristic h = new Heuristic();
        heuristic = h.heuristicFunction(board);
        return heuristic;
    }

    public void setHeuristic(int heuristic) {
        this.heuristic = heuristic;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }

    public void show() {
        for(int i = 0; i<3; i++){
            System.out.println(board[i][0] + " " + board[i][1] + " " + board[i][2]);
        }
    }

    public GameBoard getVoisin() {
        return null;
    }
}
